'use strict';

var app = angular.module('app', [
  'ngRoute', 'ngCookies',

  'app.authentication', 'home'
]).config(['$locationProvider', '$httpProvider', '$routeProvider', function($locationProvider, $httpProvider, $routeProvider) { 
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });

    $httpProvider.defaults.useXDomain = true;
    // $httpProvider.defaults.headers.common = 'Content-Type: application/json';
    // delete $httpProvider.defaults.headers.common['X-Requested-With'];

    ///////////////////////////////////////////////////////////////
    $routeProvider.when("/home", {
        templateUrl: "./app/views/home/home.html",
        controller: 'HomeController',
        reloadOnSearch: false
    });
    $routeProvider.when("/login", {
        templateUrl: "./app/views/authentication/login.html",
        controller: 'LoginController',
        controllerAs: 'vm',
        reloadOnSearch: false
    });
    $routeProvider.when("/register", {
        templateUrl: "./app/views/authentication/register.html",
        controller: 'RegisterController',
        controllerAs: 'vm',
        reloadOnSearch: false
    });
    $routeProvider.otherwise({redirectTo: '/home'});
    ///////////////////////////////////////////////////////////////
    
}]).run(['$rootScope', '$location', '$cookieStore', '$http', function ($rootScope, $location, $cookieStore, $http) {
    $rootScope.globals = $cookieStore.get('globals') || {};
    if ($rootScope.globals.currentUser) {
        $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
    }
    
    $rootScope.$on('$locationChangeStart', function (event, next, current) {
        var pages = ['/login', '/register'];
        var restrictedPage = pages.indexOf($location.path()) === -1;
        var loggedIn = $rootScope.globals.currentUser;
        if (restrictedPage && !loggedIn) {
            $location.path('/login');
        }
    });
}]).controller('appCtrl', function($scope,$http) {
     
});