"use strict";

angular
  .module('app.authentication', [])
  .controller('LoginController', ['$scope', '$location', 'AuthenticationService', 'FlashService', function($scope, $location, AuthenticationService, FlashService){
        var vm = this;

        vm.login = login;

        (function initController() {
            // reset login status
            AuthenticationService.ClearCredentials();
        })();

        function login() {
            vm.dataLoading = true;
            AuthenticationService.Login(vm.username, vm.password, function (response) {
                if (response.success) {
                    AuthenticationService.SetCredentials(vm.username, vm.password);
                    $location.path('/home');
                } else {
                    FlashService.Error(response.message);
                    vm.dataLoading = false;
                }
            });
        };

        $scope.redirect = function(url){
            $location.path(url);
        };
    }]).controller('RegisterController', ['$scope', 'UserService', '$location', '$rootScope', 'FlashService', function($scope, UserService, $location, $rootScope, FlashService){
        var vm = this;

        vm.register = register;

        function register() {
            vm.dataLoading = true;
            UserService.Create(vm.user)
                .then(function (response) {
                    if (response.success) {
                        FlashService.Success('Registration successful', true);
                        $location.path('/login');
                    } else {
                        FlashService.Error(response.message);
                        vm.dataLoading = false;
                    }
                });
        }

        $scope.redirect = function(url){
            $location.path(url);
        };
    }]);